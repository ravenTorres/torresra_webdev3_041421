<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<style>
    *{
        text-align: center;
    }
</style>


<body>

    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success">
            <div class="card-header text-center h1 text-white bg-success">
                APPEND LINE INTO A FILE
            </div>
            <div class="card-body m-2">   

            <?php
                     function appendLine($fileName, $string ,$lineNumber){
                    if (file_exists($fileName)){
                        echo "File Opened";
                        $file = fopen($fileName,'a');
                        $file2 = fwrite($file,"\n". $string);
                        fclose($file);
                    }
                }
                appendLine('file.txt',"YOU ARE GREAT!", 6);
            ?>

            </div>
        </div>
    </div>

</body>
</html>
