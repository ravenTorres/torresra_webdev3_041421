<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<style>
    *{
        text-align: center;
    }
</style>
<body>

    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success">
            <div class="card-header text-center h1 text-white bg-success">
                READ A FILE AND DISPLAY A LINE
            </div>
            <div class="card-body m-2">   

            <?php

                function randomLiner($fileName){
                    $file=file($fileName);
                    $fileName = strtolower(substr($fileName, 0, strpos($fileName,'.')));
                    $matchLetter =  preg_match_all('/[aeiou]/i',$fileName,$matches);

                    if($matchLetter % 2 == 0){
                        echo $file[5-1];
                    }else{      
                        echo $file[2-1];
                    }
                }

                randomLiner('file.txt');
            ?>

            </div>
        </div>
    </div>

</body>
</html>
