<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<style>
    *{
        text-align: center;
    }
</style>


<body>

    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success">
            <div class="card-header text-center h1 text-white bg-success">
                STRING REVERSE
            </div>
            <div class="card-body m-2">   

            <?php
                function reverseString($string){
                    if (strlen($string) % 2 == 0){
                        echo '<br>';
                        echo $string ." ---- ". strrev(substr($string, -3, strlen($string) / 2)) . "<br>";
                    }else{
                        echo $string ." ---- ". strrev($string) . "<br>";
                    }
                }
                
                reverseString("Paolul");
                reverseString("Billy");
                
            ?>


            </div>
        </div>
    </div>

</body>
</html>




