<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <title>Document</title>
</head>

<style>
    *{
        text-align: center;
    }
</style>


<body>

<div class="container d-flex justify-content-center">
    <div class="card mt-5 w-50 border-success  text-center">
        <div class="card-header h1 text-white bg-success">
             CALCULATE FACTORIAL OF A NUMBER
        </div>
        
          <div class="card-body mt-5">

            <form method="post">
              <input type="text" name="number" placeholder="Enter your desired number" class="p-2 w-75">
              <br>
              <input type="submit" name="submit" class = "btn btn-primary mt-4 mb-2" value="Check Number">
            </form>

            <?php  
            

              if (isset($_POST['submit'])) {
                  
                  if (empty($_POST['number'])) {
                      echo"<hr>";
                      echo "<h3>Answer goes here<h3>";    
                  }else{ 

                    function numfactorial($num){

                        $factorial = 1;  

                        if($num > 1){
                            for ($number = $num; $number >= 1; $number--){  
                              $factorial = $factorial * $number;  
                            }  
                        }
                        
                        echo"<hr>";
                        echo "Factorial of $num is $factorial";       

                    }

                    numfactorial($_POST['number']) ;
                       

                  }
                
              }
            ?>  

        </div>
    </div>
</div>

</body>
</html>